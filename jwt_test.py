from datetime import datetime, timedelta

import jwt

from config import DevelopmentConfig

user_data = {
    'jwi': 'ce37e70523ea42b99598f3a7645ea356',
    'iss': 'surendra',
    'typ': 'v1',
    'exp': datetime.utcnow() + timedelta(weeks=1)
}
d = DevelopmentConfig()
encoded = jwt.encode(user_data, d.SECRET_KEY, algorithm='HS256')
token = encoded.decode('utf-8')
print(token)
print('-' * 100)
decoded = jwt.decode(token, d.SECRET_KEY, algorithms=['HS256'])
print(type(decoded))
print(decoded)
