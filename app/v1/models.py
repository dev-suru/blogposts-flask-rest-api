from datetime import datetime

from flask import current_app
from flask_restplus import fields
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

from app import db
from app.v1 import v1_api


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(100), nullable=False)
    username = db.Column(db.String(50), nullable=False)
    first_name = db.Column(db.String(50), nullable=False)
    last_name = db.Column(db.String(50), nullable=False)
    email = db.Column(db.String(150), nullable=False)
    password_hash = db.Column(db.String(100), nullable=False)
    created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow())
    last_updated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow())
    blogposts = relationship('BlogPost', backref='user', lazy=True)

    @property
    def password(self):
        raise AttributeError('Password not readable')

    @password.setter
    def password(self, password):
        self.password_hash = current_app.config['CRYPTO'].encrypt(password)

    user_resource_model = v1_api.model('User', {
        'public_id': fields.String(required=True),
        'username': fields.String(required=True),
        'first_name': fields.String(required=True),
        'last_name': fields.String(required=True),
        'email': fields.String(required=True),
        'last_updated': fields.DateTime(required=True),
    })

    login_user_request_resource_model = v1_api.model('Login User Request', {
        'username': fields.String(required=True),
        'password': fields.String(required=True)
    })

    register_user_request_resource_model = v1_api.clone('Register User Request', login_user_request_resource_model, {
        'first_name': fields.String(required=True),
        'last_name': fields.String(required=True),
        'email': fields.String(required=True),
    })

    login_user_response_resource_model = v1_api.clone('Login User Response', user_resource_model, {
        'token': fields.String(required=False)
    })


class BlogPost(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(512), nullable=False)
    post = db.Column(db.String(1024), nullable=False)
    user_id = db.Column(db.Integer, ForeignKey('user.id'))
    created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow())
    last_updated = db.Column(db.DateTime, nullable=False, default=datetime.utcnow())

    create_blogpost_request_resource_model = v1_api.model('Create BlogPost', {
        'title': fields.String(required=True),
        'post': fields.String(required=True)
    })

    create_blogpost_response_resource_model = v1_api.model('Create BlogPost Response', {
        'id': fields.Integer(required=True)
    })

    update_blogpost_request_resource_model = v1_api.clone('Update BlogPost', create_blogpost_request_resource_model, {
        'id': fields.Integer(required=True)
    })

    blogpost_resource_model = v1_api.clone('BlogPost', create_blogpost_request_resource_model, {
        'id': fields.Integer(required=True),
        'last_updated': fields.DateTime(required=True)
    })

    blogpost_all_resource_model = v1_api.clone('BlogPost', blogpost_resource_model, {
        'user.public_id': fields.String(required=True)
    })
