from http import HTTPStatus

import jwt
from flask import request, current_app

from app import logger
from . import v1_api
from .models import User


# required_token decorator
def jwt_required(f):
    def wrapper(*args, **kwargs):
        logger.info('got a request')
        auth_header = request.headers.get('Authorization')
        current_user = None
        if auth_header:
            try:
                token = jwt.decode(auth_header, current_app.config['SECRET_KEY'])
                current_user = User.query.filter_by(public_id=token['jwi']).first()
                if not current_user:
                    raise
            except jwt.ExpiredSignatureError as e:
                raise e
            except (jwt.DecodeError, jwt.InvalidTokenError) as e:
                raise e
            except:
                v1_api.abort(HTTPStatus.UNAUTHORIZED, 'Unknown token error')
        else:
            v1_api.abort(HTTPStatus.FORBIDDEN, 'Token required')
        return f(*args, **kwargs, current_user=current_user)

    wrapper.__doc__ = f.__doc__
    wrapper.__name__ = f.__name__
    return wrapper
