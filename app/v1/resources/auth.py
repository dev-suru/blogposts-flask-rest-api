from http import HTTPStatus

from flask_restplus import Namespace, Resource

from app import db, logger
from app.v1 import v1_api
from app.v1.models import User
from exceptions import FieldValidationException, UnauthorizedException
from global_utils import validate_field, decrypt, create_jwt, get_uuid

auth_ns = Namespace('auth')


@auth_ns.route('/register')
class Register(Resource):

    @auth_ns.expect(User.register_user_request_resource_model, validate=True)
    @auth_ns.marshal_with(User.user_resource_model, code=HTTPStatus.CREATED, description="User created successfully")
    @auth_ns.response(HTTPStatus.BAD_REQUEST, 'Invalid user data')
    def post(self):
        payload = v1_api.payload
        validate_field('username', payload['username'])
        validate_field('email', payload['email'])
        validate_field('password', payload['password'])

        if User.query.filter_by(username=payload['username']).filter_by(email=payload['email']).first():
            raise FieldValidationException(error_field_name='username/email', message='User already exist')

        user = User()
        user.username = payload['username']
        user.password = payload['password']
        user.first_name = payload['first_name']
        user.last_name = payload['last_name']
        user.email = payload['email']
        user.public_id = get_uuid()
        db.session.add(user)
        db.session.commit()
        return user


@auth_ns.route('/login')
class Login(Resource):

    @auth_ns.expect(User.login_user_request_resource_model, validate=True)
    @auth_ns.marshal_with(User.login_user_response_resource_model, code=HTTPStatus.OK, description="User login success")
    @auth_ns.response(HTTPStatus.UNAUTHORIZED, 'Invalid username or password')
    def post(self):
        user = User.query.filter_by(username=v1_api.payload['username']).first()
        if user and v1_api.payload['password'] == decrypt(user.password_hash):
            user.token = create_jwt(user.public_id)
            logger.debug('user login')
            logger.info('user login prod')
            return user
        else:
            raise UnauthorizedException()
