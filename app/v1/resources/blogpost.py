from http import HTTPStatus

from flask_restplus import Resource, Namespace

from app import db
from app.v1 import v1_api
from app.v1.models import BlogPost
from app.v1.utils import jwt_required
from app import logger

blogpost_ns = Namespace('blogpost')


@blogpost_ns.route('/')
class BlogPostOp(Resource):

    @blogpost_ns.doc('BlogPost data', security='User JWT')
    @blogpost_ns.marshal_with(BlogPost.blogpost_resource_model, as_list=True, code=HTTPStatus.OK, description="Get all BlogPosts for user")
    @jwt_required
    def get(self, current_user):
        logger.info('process rq')
        return BlogPost.query.filter_by(user=current_user).all()

    @blogpost_ns.doc('BlogPost data', security='User JWT', body=BlogPost.create_blogpost_request_resource_model, validate=True)
    @blogpost_ns.marshal_with(BlogPost.create_blogpost_response_resource_model, code=HTTPStatus.CREATED, description="BlogPost created")
    @blogpost_ns.response(HTTPStatus.BAD_REQUEST, 'Invalid BlogPost data')
    @jwt_required
    def post(self, current_user):
        blogpost = BlogPost(title=v1_api.payload['title'], post=v1_api.payload['post'], user=current_user)
        db.session.add(blogpost)
        db.session.commit()
        return blogpost, HTTPStatus.CREATED

    @blogpost_ns.doc('BlogPost data', security='User JWT', body=BlogPost.update_blogpost_request_resource_model, validate=True)
    @blogpost_ns.response(HTTPStatus.BAD_REQUEST, 'Invalid BlogPost data')
    @blogpost_ns.response(HTTPStatus.OK, 'BlogPost updated successfully')
    @jwt_required
    def put(self, current_user):
        blogpost = BlogPost.query.filter_by(user=current_user, id=v1_api.payload['id']).first()
        if blogpost:
            from datetime import datetime
            blogpost.last_updated = datetime.utcnow()
            blogpost.title = v1_api.payload['title']
            blogpost.post = v1_api.payload['post']
            db.session.commit()
            return None, HTTPStatus.OK
        else:
            return None, HTTPStatus.BAD_REQUEST


@blogpost_ns.route('/<id>')
class BlogPostDel(Resource):

    @blogpost_ns.doc('BlogPost data', security='User JWT')
    @blogpost_ns.response(HTTPStatus.NOT_FOUND, 'BlogPost not found')
    @blogpost_ns.response(HTTPStatus.OK, 'BlogPost deleted successfully')
    @blogpost_ns.param('id', 'BlogPost id')
    @jwt_required
    def delete(self, current_user, id):
        blogpost = BlogPost.query.filter_by(user=current_user, id=id).first()
        if blogpost:
            db.session.delete(blogpost)
            db.session.commit()
            return blogpost.id, HTTPStatus.OK
        else:
            return None, HTTPStatus.NOT_FOUND


@blogpost_ns.route('/all')
class BlogPostAll(Resource):

    @blogpost_ns.doc('BlogPost data', security='User JWT')
    @blogpost_ns.marshal_with(BlogPost.blogpost_all_resource_model, as_list=True, code=HTTPStatus.OK, description="Get all BlogPosts for all users")
    @jwt_required
    def get(self, current_user):
        return BlogPost.query.all()
