from http import HTTPStatus

import jwt
from flask import Blueprint
from flask_restplus import Api

from exceptions import FieldValidationException, UnauthorizedException
from global_utils import authorizations

v1_blueprint = Blueprint('v1_blueprint', __name__)
# doc = '/doc/' if current_app.config['DEBUG'] else False
v1_api = Api(v1_blueprint, title='Blog Posts', version='1.0', description='The Blog Posts API', authorizations=authorizations, doc='/doc/')

from .resources.auth import auth_ns
from .resources.blogpost import blogpost_ns


@v1_api.errorhandler(FieldValidationException)
def handle_field_validation_exception(error):
    error_message = {
        'message': 'Field validation error',
        'errors': {
            error.error_field_name: error.message
        }
    }
    return error_message, HTTPStatus.BAD_REQUEST


@v1_api.errorhandler(jwt.ExpiredSignatureError)
def handle_expired_signature_error(error):
    error_message = {
        'message': 'Token expired'
    }
    return error_message, HTTPStatus.UNAUTHORIZED


@v1_api.errorhandler(jwt.InvalidTokenError)
@v1_api.errorhandler(jwt.DecodeError)
@v1_api.errorhandler(jwt.InvalidIssuerError)
def handle_invalid_token_error(error):
    error_message = {
        'message': 'Token incorrect, supplied or malformed'
    }
    return error_message, HTTPStatus.UNAUTHORIZED


@v1_api.errorhandler(UnauthorizedException)
def handle_unauthorized_exception(error):
    error_message = {
        'message': error.message
    }
    return error_message, HTTPStatus.UNAUTHORIZED


v1_api.add_namespace(auth_ns)
v1_api.add_namespace(blogpost_ns)
