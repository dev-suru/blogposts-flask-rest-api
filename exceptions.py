class FieldValidationException(Exception):
    def __init__(self, message='Field validation error', error_field_name='unknown_field', *args, **kwargs):
        super().__init__(args, **kwargs)
        self.error_field_name = error_field_name
        self.message = message


class UnauthorizedException(Exception):
    def __init__(self, message='User is not authorized', *args, **kwargs):
        super().__init__(args, **kwargs)
        self.message = message
