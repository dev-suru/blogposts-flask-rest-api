import os

from flask import request, g
from flask_script import Manager

from app import create_app, db
from global_utils import get_uuid

app = create_app(os.getenv('APP_ENV', 'dev'))
manager = Manager(app)


@app.before_request
def before_flask_request():
    request_id = request.headers.get('X-Request-Id')
    g.request_id = get_uuid(request_id)


# @app.after_request
# def log_request(response):
#     ip = g.request_id
#     app.logger.info(str(ip))
#     return response


@manager.command
def run():
    app.run()


@manager.command
def create_db():
    print('creating database schema')
    db.create_all()
    print('done creating database schema')


@manager.command
def drop_db():
    db.drop_all()


if __name__ == '__main__':
    manager.run()
