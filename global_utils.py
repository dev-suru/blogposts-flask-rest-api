import re
import uuid
from datetime import datetime, timedelta

import jwt
from cryptography.fernet import Fernet
from flask import current_app


class AppCrypto:
    def __init__(self, key):
        self._f = Fernet(key.encode('utf-8'))

    def encrypt(self, data):
        return self._f.encrypt(str(data).encode('utf-8')).decode('utf-8')

    def decrypt(self, data):
        return self._f.decrypt(str(data).encode('utf-8')).decode('utf-8')


def validate_field(field, data):
    from exceptions import FieldValidationException
    regx = {
        'username': r'^\w{1}[a-zA-Z0-9]{4,31}$',
        'password': r'^\S{6,32}$',
        'email': r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)'
    }
    if not re.search(regx[field], data):
        raise FieldValidationException(error_field_name=field, message='Invalid value')


def encrypt(data):
    return current_app.config['CRYPTO'].encrypt(data)


def decrypt(data):
    return current_app.config['CRYPTO'].decrypt(data)


authorizations = {
    'User JWT': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    }
}


def create_jwt(public_id):
    user_data = {
        'jwi': public_id,
        'iss': 'surendra',
        'typ': 'v1',
        'exp': datetime.utcnow() + timedelta(weeks=1)
    }
    encoded = jwt.encode(user_data, current_app.config['SECRET_KEY'], algorithm='HS256')
    return encoded.decode('utf-8')


def get_uuid(original_request_id=None):
    r_id = uuid.uuid4().hex
    if original_request_id:
        r_id = f'{original_request_id},{r_id}'
    return r_id
