import logging
import sys
from logging.handlers import TimedRotatingFileHandler

from flask import g


class RequestIdFilter(logging.Filter):
    def filter(self, record):
        record.request_id = g.request_id if g.request_id else ''
        return True


def setup(app):
    app.logger.addFilter(RequestIdFilter())
    formatter = logging.Formatter("[%(asctime)s] %(request_id)s {%(name)s | %(filename)s:%(lineno)d} - %(levelname)s - %(message)s")

    file_handler = TimedRotatingFileHandler(app.config['LOG_FILE'], when='midnight')
    file_handler.setFormatter(formatter)
    file_handler.setLevel(logging.INFO)

    app.logger.setLevel(logging.INFO)

    if app.config['DEBUG']:
        app.logger.setLevel(logging.DEBUG)

        file_handler.setLevel(logging.DEBUG)
        console_handler = logging.StreamHandler(sys.stdout)
        console_handler.setLevel(logging.DEBUG)
        console_handler.setFormatter(formatter)
        app.logger.addHandler(console_handler)

    app.logger.addHandler(file_handler)
