from global_utils import AppCrypto


class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = ''
    CRYPTO = None
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    LOG_FILE = '/tmp/flask.log'


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://surendra:password@localhost/flask_dev'
    SECRET_KEY = 'bacd97a8b7f883c4985c9ee736a0c2c4f4d7bb8fce5966089ea56800a700fb5f'
    CRYPTO = AppCrypto('duicrDeW-XHcBBg60gA_9q9J-jYAnOYq7qOTGItUv5o=')


class ProdConfig(Config):
    # SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://surendra:password@localhost/flask_testing'
    # SECRET_KEY = 'cfc707a6ae265b753c363e82721a31638791d2555fb62e11f2bd1ec034ef3632'
    # CRYPTO = AppCrypto('OIJV1skqFAF3c6sKbHSbNANTJz7h5QgJ40eiowyP0hM=')
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://surendra:password@localhost/flask_dev'
    SECRET_KEY = 'bacd97a8b7f883c4985c9ee736a0c2c4f4d7bb8fce5966089ea56800a700fb5f'
    CRYPTO = AppCrypto('duicrDeW-XHcBBg60gA_9q9J-jYAnOYq7qOTGItUv5o=')


config = {
    'dev': DevelopmentConfig,
    'prod': ProdConfig
}
