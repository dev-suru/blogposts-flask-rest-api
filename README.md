## How to setup

1. Clone the repo
2. `virtualenv -p python3 .venv`
3. `source .venv/bin/activate`
4. `pip install -r requirements.txt`
5. install postgres run locally and create a database `flask_dev` and change db settings in `config.py`
6. `python3 main.py create_db`
7. `python3 main.py run`

[Swagger UI](http://127.0.0.1:5000/api/v1/doc/)